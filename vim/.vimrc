" Author: Brian (xangelux) Martínez
" Description: Configuration file for Vim
" Contact: xangelux at gmail dot com

" Prelude {{{
  scriptencoding 'utf-8'
  autocmd! bufwritepost .vimrc source %
  autocmd FocusLost * :silent! wall
  autocmd VimResized * :wincmd =
  set path+=** " to use find on files
  set fileencoding=utf-8
  set termencoding=utf-8
  " let mapleader=","
  let g:netrw_sort_by = "exten"
  let g:netrw_liststyle = 3
  let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
  set spell spelllang=es_mx,en
  imap kj <ESC>
  imap Kj <ESC>
  imap KJ <ESC>
  nmap <C-o> i<CR><Esc>
  nnoremap <leader>W mz:%s/\s\+$//<CR>:let @/=''<CR>`z
  nnoremap <leader><Space> :noh<CR>
  command! W w
  command! Q q
  cmap wq w
  command! Qa qa
  command! QA qa
  set backupdir=$HOME/.vim/temp/backup/
  set directory=$HOME/.vim/temp/swap/
  set viewdir=$HOME/.vim/temp/view/
  set undodir=$HOME/.vim/temp/undo/
  set backup
  set noswapfile
  set undofile
  set nocompatible
  " This option should be after nocompatible
  " '50       -> Save 50 marks
  " n$HOME... -> Where the viminfo file will be saved
  set viminfo='50,<800,/50,h,n$HOME/.vim/temp/viminfo " This line can cause errors in vim
  if !isdirectory(expand(&backupdir))
    call mkdir(expand(&backupdir), "p")
  endif
  if !isdirectory(expand(&directory))
    call mkdir(expand(&directory), "p")
  endif
  if !isdirectory(expand(&undodir))
    call mkdir(expand(&undodir), "p")
  endif
  if !isdirectory(expand(&viewdir))
    call mkdir(expand(&viewdir), "p")
  endif
  set autowrite
  set history=1000
  set timeoutlen=300
  set clipboard+=unnamed
  set autoread
  set viewoptions=folds,options,cursor,unix,slash " For mkview
  autocmd filetype crontab setlocal nobackup nowritebackup " So it works with crontab
" }}}

" Visual {{{

  set number
  set relativenumber
  syntax on
  set regexpengine=1 " Old engine, Ruby slows down on the new one
  set synmaxcol=200
  set list
  set listchars=tab:▸\ ,trail:·,eol:¬,extends:»,precedes:«,nbsp:+
  set colorcolumn=81
  set fillchars=diff:⣿,vert:│
  set showbreak=↪
  set cursorline
  set wrap
  set nrformats-=octal
  set encoding=utf-8
  set incsearch
  set showcmd
  set laststatus=2
  set showmode
  set lazyredraw
  set diffopt+=iwhite,vertical
  set hlsearch
  set ignorecase
  set smartcase

  " The font has updated icons
  set guifont=Anonymice\ Nerd\ Font\ Complete\ Mono:h12
  "set guifont=Dejavu\ Sans\ Mono\ for\ Powerline\ Nerd\ Font\ Complete:h12
  set ruler " Line and column number of the cursor position
  set guioptions+=aceLlRrb " Need to add LlRrb to remove it
  " a: Selection is available to be pasted on the system and is yanked to "* reg
  " c: Console dialogs instead of popups
  " e: Show tabline
  set guioptions-=LlRrb
  set scrolloff=3
  set sidescroll=1
  set sidescrolloff=6
  set foldlevelstart=0
  set textwidth=80

  set showmatch   " Show matching parens when closing
  set matchtime=6 " Show for this amount of tenth's of a second

  " tmux will only forward escape sequences to the terminal if surrounded by a DCS sequence
  " http://sourceforge.net/mailarchive/forum.php?thread_name=AANLkTinkbdoZ8eNR1X2UobLTeww1jFrvfJxTMfKSq-L%2B%40mail.gmail.com&forum_name=tmux-users

  if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
  else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
  endif

  "
  " Formatting options:
  "
  " q Allow formatting of comments using gq
  " r Automatically insert the current comment leader after new line
  " n When formatting text, recognize numbered lists
  " l Long lines are not broken in insert mode
  " 2 When formatting text, use the indent of the second line of a paragraph
  "   for the rest of the paragraph
  " j Where it makes sense, remove a comment leader when joining lines
  " t Auto-wrap text using textwith
  " c Auto-wrap comments using textwidth, inserting the current comment leader
  "   automatically.
  " o Automatically insert the current commen leader after hitting o or O
  " v Only break a line at a blank char
  "

  set formatoptions=qrnl2jtcov

" }}}

" Indentation {{{
  set autoindent  " Copy indent in new line
  set smartindent " New indent level if needed
  set expandtab
  set softtabstop=2
  set smarttab
  set tabstop=2
  set shiftwidth=2 " Width to indent for >> and << operators
  set backspace=indent,eol,start
  set shiftround
"}}}

" Folds {{{
  nnoremap <Space> za
  vnoremap <Space> za
  augroup ft_vim
    au!
    au FileType vim setlocal foldmethod=marker
    au FileType help setlocal textwidth=78
  augroup END
" }}}

" Navigation {{{
  nmap <C-j> <C-w>j
  nmap <C-k> <C-w>k
  nmap <C-l> <C-w>l
  nmap <C-h> <C-w>h
  " Disabling keys
  nmap <Up> <NOP>
  nmap <Down> <NOP>
  nmap <Left> <NOP>
  nmap <Right> <NOP>
  " In insert too
  imap <Up> <NOP>
  imap <Down> <NOP>
  imap <Left> <NOP>
  imap <Right> <NOP>
" }}}

" Plugins {{{

" Try to load minpac.
silent! packadd minpac " Small non-intrusive package manager

if exists('*minpac#init')

  " minpac is available.
  call minpac#init()
  call minpac#add('k-takata/minpac', {'type': 'opt'})

  call minpac#add('junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' })
  call minpac#add('junegunn/fzf.vim')

  autocmd FileType sls call minpac#add('saltstack/salt-vim')

  " Tim Pope section
  call minpac#add('tpope/vim-fugitive') " Git integration
  call minpac#add('tpope/vim-repeat')   " To repeat things that plugins do
  call minpac#add('tpope/vim-surround') " Surround text objects with chars
  call minpac#add('tpope/vim-eunuch')   " Unix Commands from VIM
  call minpac#add('tpope/vim-endwise')  " Add closing on missing constructs
  call minpac#add('tpope/vim-rvm')      " RVM commands from VIM
  call minpac#add('tpope/vim-rails')    " Rails navigation between files
  call minpac#add('tpope/vim-commentary') " Comment text objects
  call minpac#add('tpope/vim-jdaddy')   " JSON manipulation
  call minpac#add('tpope/vim-dispatch') " Send to command line and parse
  call minpac#add('tpope/vim-sleuth')   " Switch indent depending on ft

  call minpac#add('vim-ruby/vim-ruby')  " Ruby text Objects, motions and indent

  call minpac#add('vim-scripts/AnsiEsc.vim') " Color ANSI escape chars

  call minpac#add('tomasr/molokai')     " Colorscheme
  call minpac#add('morhetz/gruvbox')    " Colorscheme
  call minpac#add('altercation/vim-colors-solarized') " Colorscheme

  call minpac#add('junegunn/vim-easy-align')
  call minpac#add('sjl/gundo.vim')      " Visual Undo tree
  call minpac#add('Yggdroot/indentLine')
  call minpac#add('mileszs/ack.vim')
  call minpac#add('sheerun/vim-polyglot') " Most programming languages support
  call minpac#add('andreshazard/vim-logreview') " Log reviewer
  call minpac#add('vim-airline/vim-airline') " Powerline for Vim
  call minpac#add('vim-airline/vim-airline-themes')
  call minpac#add('ryanoasis/vim-devicons')
  call minpac#add('andymass/vim-matchup')

  call minpac#add('stoicAlchemist/vim-evernote')
  "call minpac#add('b4b4r07/vim-hcl') "Hashicorp Vault config language

" Plugin Configs {{{
  " Ack configs {{{
    " Use AG instead of ACK
    let g:ackprg = 'ag --vimgrep --smart-case'
  " }}}
  " Autocomplete options {{{
    set complete=.,w,b,u,t
    set completeopt=longest,menuone,preview
    " Ignore directories and files
    set wildignore+=*.DS_Store
    set wildignore+=*/_build**
    set wildignore+=*/.ssh
    set wildignore+=*/.npm
    set wildignore+=*cache/**

    " For snippet_complete marker.
    if has('conceal')
      set conceallevel=2 concealcursor=i
    endif
  " }}}
  " Colorscheme {{{
    set t_Co=256

    colorscheme gruvbox
    set background=dark
    let g:solarized_termcolors=256

    if has('gui_macvim')
      hi clear SpellBad
      hi SpellBad cterm=underline
    endif
  " }}}
  " EasyAlign {{{
    vmap <Enter> <Plug>(EasyAlign)
  " }}}
  " IndentLine {{{
    let g:indent_guides_start_level=2
    let g:indent_guides_guide_size=1
    "let g:indentLine_char='│'
    let g:indentLine_char='┆'
  " }}}
  " DevIcons {{{

  if !exists('g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols')
    let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols = {}
  endif

  if !exists('g:WebDevIconsUnicodeDecorateFileNodesExactSymbols')
    " do not remove: exact-match-case-sensitive-*
    let g:WebDevIconsUnicodeDecorateFileNodesExactSymbols = {}
  endif

  if !exists('g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols')
    let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols = {}
  endif
  let g:webdevicons_enable_airline_statusline = 1
  let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['evernote'] = ''
  let g:WebDevIconsUnicodeDecorateFileNodesExactSymbols['__evernote__'] = ''
  let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols['.*\.evernote$'] = ''
  " }}}
  " Airline {{{
    if !exists('g:airline_symbols')
      let g:airline_symbols = {}
      let g:airline_powerline_fonts = 1
    endif
    let g:airline_left_sep          = '⮀'
    let g:airline_left_alt_sep      = '⮁'
    let g:airline_right_sep         = '⮂'
    let g:airline_right_alt_sep     = '⮃'
    let g:airline_symbols.branch    = '⭠'
    let g:airline_symbols.readonly  = '⭤'
    let g:airline_symbols.linenr    = '⭡'

  " FZF {{{
    nmap <leader>f :Files<CR>
    nmap <leader>b :Buffers<CR>
  " }}}

  " Evernote {{{
    nnoremap <leader>ec :EvernoteVsplitCreateNote<CR>
    nnoremap <leader>en :EvernoteGetNotebookList<CR>
  " }}}
" }}}"

endif

  filetype plugin indent on

" }}}

