unbind C-b
set -g prefix C-Space
set-option -sg escape-time 10 # Neovim improvement

# Always open new window with current path
bind c new-window -c "#{pane_current_path}"
bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
bind Space send-prefix
set-option -g default-shell $SHELL
set-option -g default-command "reattach-to-user-namespace -l ${SHELL}"
# Use vim keybindings in copy mode
setw -g mode-keys vi

# History limit
set-option -g history-limit 10000
# Setup 'v' to begin selection as in Vim
set-option -g mouse on
set -g default-terminal "xterm-256color"
set -g terminal-overrides ',xterm-256color:Tc' # True colors in vim
bind -Tcopy-mode-vi v send -X begin-selection
bind -Tcopy-mode-vi y send -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"

# Update default binding of `Enter` to also use copy-pipe
unbind -Tcopy-mode-vi Enter

# Bind ']' to use pbpaste
bind ] run "reattach-to-user-namespace pbpaste | tmux load-buffer - && tmux paste-buffer"
#### COLOUR (Solarized 256)

# default statusbar colors
set-option -g status-bg colour235 #base02
set-option -g status-fg colour136 #yellow
set-option -g status-attr default

# default window title colors
set-window-option -g window-status-fg colour244 #base0
set-window-option -g window-status-bg default
#set-window-option -g window-status-attr dim

# active window title colors
set-window-option -g window-status-current-fg colour166 #orange
set-window-option -g window-status-current-bg default
#set-window-option -g window-status-current-attr bright

# pane border
set-option -g pane-border-fg colour235 #base02
set-option -g pane-active-border-fg colour240 #base01

# message text
set-option -g message-bg colour235 #base02
set-option -g message-fg colour166 #orange

# pane number display
set-option -g display-panes-active-colour colour33 #blue
set-option -g display-panes-colour colour166 #orange

# clock
set-window-option -g clock-mode-colour colour64 #green

# bell
set-window-option -g window-status-bell-style fg=colour235,bg=colour160 #base02, red

# Traverse panes
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Start index in 1 instead of 0
set -g base-index 1
setw -g pane-base-index 1
