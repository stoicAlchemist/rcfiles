# Info: To set configurations for the completion system. This line is setting
#       the filename module to the string passed
# Help: man zshcompsys
zstyle :compinstall filename "${HOME}/.zshrc"

autoload -Uz colors vcs_info
autoload -Uz compinit
compinit
setopt prompt_subst
colors
autoload -Uz promptinit
promptinit

# VCS styling
zstyle ':vcs_info:*' actionformats \
  ' %F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f'
zstyle ':vcs_info:*' formats \
  ' %F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f%m%u%c'
#zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true

precmd () {
  vcs_info
  RPROMPT="%(1j,%{$fg[red]%},%{$fg[black]%})(%j)%(0?,%{$fg[black]%},%{$fg[red]%}) [%?]%{$reset_color%} %B%*%b    "
}

# ZLE is the ZSH Line Editor that allows to edit command lines before execute
# Vim mode
zle-keymap-select(){
  RPROMPT="%(1j,%{$fg[red]%},%{$fg[black]%})(%j)%(0?,%{$fg[black]%},%{$fg[red]%}) [%?]%{$reset_color%} %B%*%b"
  [[ $KEYMAP = vicmd ]] && RPROMPT="%{$fg[green]%}CMD%{$reset_color%}%(1j,%{$fg[red]%},%{$fg[black]%})(%j)%(0?,%{$fg[black]%},%{$fg[red]%}) [%?]%{$reset_color%} %B%*%b"
  () { return $__prompt_status }
  zle reset-prompt
}

zle-line-init() {
  typeset -g __prompt_status="$?"
}
zle -N zle-keymap-select
zle -N zle-line-init

  PROMPT='%(4L,\\___,)%B%(0#,%{$fg[red]%}%n%#,%{$fg[blue]%})%b%{$fg[white]%}%1/%{$reset_color%}${vcs_info_msg_0_} %(0#,%{$fg[red]%}☢,%{$fg[yellow]%}>%{$fg[yellow]%}>%{$fg[red]%}>)%{$reset_color%} '

# Directory stack management
setopt auto_pushd
setopt correct_all # Syntax correct
unsetopt correct_all
setopt inc_append_history # Save command into history before executing
setopt share_history
setopt auto_menu # Automatically complete with first option on tab twice
setopt always_to_end # Whenever autocomplete occurs, send cursor to the end
setopt auto_list # Give options whenever autocomplete is ambiguos
setopt auto_name_dirs # automaticaly expand variables on cd
setopt cdablevars
setopt auto_remove_slash # remove slash if in autocomplete the user gives imputs
setopt auto_param_slash # Add a slash if the autocomplete is a dir name
setopt complete_aliases # Prevent alias internally substituted
setopt glob_complete # Show options instead of expand on regex autocomplete
setopt pushdminus # Use cd +2 instead of cd -2 on directory stack
setopt autocd
setopt interactivecomments # Use comments with '#'
setopt noclobber # Do not overwrite files unless >! is used
setopt HIST_REDUCE_BLANKS # Save space by ignoring blanks on commands
setopt HIST_IGNORE_SPACE # Ignore command on history if prepended with space
setopt HIST_IGNORE_DUPS
#setopt EXTENDEDGLOB
setopt APPEND_HISTORY # Append history instead of replace
setopt NOMATCH # Print error if no match found for autocomplete
setopt NOTIFY # Notify status of jobs, no need to wait

bindkey -v
bindkey -M viins 'Ç' vi-cmd-mode
bindkey "\e[1~" beginning-of-line # Home
bindkey "\e[4~" end-of-line # End
bindkey "\e[5~" beginning-of-history # PageUp
bindkey "\e[6~" end-of-history # PageDown
bindkey "\e[2~" quoted-insert # Ins
bindkey "\e[3~" delete-char # Del
bindkey "\e[1;5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[1;5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey "\e[Z" reverse-menu-complete # Shift+Tab

if [[ $TERM_PROGRAM == 'rxvt' ]]
then
  # for rxvt
  bindkey "\e[7~" beginning-of-line # Home
  bindkey "\e[8~" end-of-line # End
fi

# for non RH/Debian xterm, can't hurt for RH/Debian xterm
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line

if [[ $OSTYPE == 'freebsd' ]]
then
  # for freebsd console
  bindkey "\e[H" beginning-of-line
  bindkey "\e[F" end-of-line
fi

# for <ctrl>+r history search
bindkey "^R" history-incremental-search-backward
bindkey -M viins '^s' history-incremental-search-backward
bindkey -M vicmd '^s' history-incremental-search-backward

# History
bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward

# vim key bindings
bindkey -a 'gg' beginning-of-buffer-or-history
bindkey -a 'g~' vi-oper-swap-case
bindkey -a G end-of-buffer-or-history
bindkey -a u undo
bindkey -a '^R' redo
bindkey '^?' backward-delete-char
bindkey '^H' backward-delete-char

# My Alias

# Force tmux to use 256 colors
alias tmux='tmux -2'

alias nc='nocorrect'
alias grep='grep --color=auto'

if [[ $OSTYPE == "darwin"* ]]
then
  alias ctags="`brew --prefix`/bin/ctags"
  alias ls='gls -p --color=auto --group-directories-first'
  alias ll='gls -lap --color=auto --group-directories-first'
fi

if [[ $OSTYPE == 'linux' ]]
then
  alias ls='ls -p --color=auto --group-directories-first'
  alias ll='ls -lap --color=auto --group-directories-first'
  # Archlinux
  alias pin='sudo pacman -S'
  alias prm='sudo pacman -Rsn'
  alias pac='sudo pacman -U'
  alias pss='pacman -Ss'
  alias pup='sudo pacman -Syu'
  alias pqi='pacman -Qi'
  alias pqs='pacman -Qs'
  alias psi='pacman -Si'
fi

alias dh='dirs -v' # Print Dir Stack
alias suvim='sudo vim'
alias sucat='sudo cat'
alias psa='ps auxw'
alias :Qa='exit'
alias :qa='exit'
alias :q='exit'

alias git="nocorrect git"
alias vim="nocorrect vim"
alias nvim="nocorrect nvim"
export NVIM_TUI_ENABLE_TRUE_COLOR=1

# My exports

export EDITOR="nvim"
export SUDO_EDITOR='rnvim'
export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000000
export PATH=~/bin:$PATH:.:$HOME/.gopath/bin
export GOPATH=~/.gopath

# Less Colors for Man Pages
export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\E[0m'           # end mode
export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
#export LESS_TERMCAP_so=$'\E[38;5;246m'    # begin standout-mode - info box
export LESS_TERMCAP_so=$'\E[31m'    # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\E[0m'           # end underline
export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline

# Tmux and vim colors in terminal
export TERM=screen-256color-bce

# Syntax highlighting
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

if [[ $TERM_PROGRAM == "iTerm.app" ]]
then
  [[ -e "${HOME}/.iterm2_shell_integration.zsh" ]] &&\
    . "${HOME}/.iterm2_shell_integration.zsh"
fi

#export NVM_DIR="${HOME}/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"  # This loads nvm

export NVM_DIR="$HOME/.nvm"
alias loadNvm='. "/usr/local/opt/nvm/nvm.sh"'

export PATH="/usr/local/opt/elasticsearch@1.7/bin:$PATH"

# fzf for fuzzy finding
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# All external custom configs
source ~/.zsh/contexts/*.zsh

# Hooks and functions
function chpwd() {
  if [ -r $PWD/.env ]; then
    source $PWD/.env
  fi
}

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

# To load JENV for managing Java versions
eval "$(jenv init -)"
